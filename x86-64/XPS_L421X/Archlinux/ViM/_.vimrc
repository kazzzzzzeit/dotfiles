" Location: $HOME/.vimrc
" Description: Configuration file for ViM.



" VUNDLE SETTINGS
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
  Plugin 'VundleVim/Vundle.vim'
  Plugin 'scrooloose/syntastic'
  Plugin 'tpope/vim-surround'
  Plugin 'godlygeek/tabular'
  Plugin 'Townk/vim-autoclose'
  Plugin 'jlanzarotta/bufexplorer'
  Plugin 'tpope/vim-fugitive'
  Plugin 'tpope/vim-git'
  Plugin 'scrooloose/nerdtree'
  Plugin 'Xuyuanp/nerdtree-git-plugin'
  Plugin 'vim-scripts/taglist.vim'
  Plugin 'easymotion/vim-easymotion'
  Plugin 'vim-airline/vim-airline'
  Plugin 'vim-airline/vim-airline-themes'
  Plugin 'nanotech/jellybeans.vim'
call vundle#end()
filetype plugin indent on



" AIRLINE SETTINGS
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_theme = 'bubblegum'



" GENERAL SETTINGS
set number
set nocompatible
set noexrc
set cursorline
set noerrorbells
set novisualbell
set encoding=utf-8
set fileencoding=utf-8
set termencoding=utf-8
set nobackup
set nowritebackup
set noswapfile
set title
set ruler
set noendofline
set nofixendofline



" SEARCH SETTINGS
set incsearch
set hlsearch



" COLOR SETTINGS
syntax on
filetype plugin on
set t_Co=256
set background=dark
colorscheme jellybeans



" TAB SETTINGS
set expandtab
set tabstop=2
set shiftwidth=2
set autoindent
set smartindent
set shiftround



" HOTKEY/MACRO SETTINGS
set pastetoggle=<F2>
nmap <F3> :!./%<CR>
nmap <F4> :w<CR>:!clang -Wall -Wextra -g -std=c11 -o ./%:r %<CR>
nmap <F5> :!./%:r<CR>



" FILE SPECIFIC SETTINGS
autocmd FileType html setlocal noeol binary fileformat=dos
augroup project
  autocmd!
  autocmd BufRead,BufNewFile *.h,*.c set filetype=c.doxygen
augroup END



" PROGRAM SPECIFIC SETTINGS
au BufRead /tmp/mutt-* set tw=72



" PERFORMANCE
set lazyredraw
syntax sync maxlines=256



" NEEDS TO BE LAST?
set viminfo=""
