#!/bin/bash
# Location: $HOME/bin/start-toronly.sh
# Description: This script will start a VM configured to use network devices
#              that will restrict it to only connect to the Internet via the
#              TOR network.



QEMU_AUDIO_DRV="pa"
ISO="$HOME/software/operating_systems/linux/archlinux/archlinux-2017.07.01-x86_64.iso"
DRIVE="/dev/mapper/data-toronly"



qemu-system-x86_64                                                      \
  -cpu host                                                             \
  -machine accel=kvm                                                    \
  -smp 2                                                                \
  -m 2G                                                                 \
  -cdrom "$ISO"                                                         \
  -boot order=c                                                         \
  -vga virtio                                                           \
  -soundhw hda                                                          \
  -usbdevice tablet                                                     \
  -monitor stdio                                                        \
  -display gtk                                                          \
  -drive file="$DRIVE",cache=none,if=virtio,format=raw,aio=native       \
  -net nic,model=virtio -net tap,ifname=tornet,script=no,downscript=no  \
  -name "TOR-ONLY VM"
