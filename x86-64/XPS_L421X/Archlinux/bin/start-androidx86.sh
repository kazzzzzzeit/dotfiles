#!/bin/sh
# Location: $HOME/bin/start-androidx86.sh
# Description: This script sets up a VM to run Remix-OS.



QEMU_AUDIO_DRV=pa
HDD="$HOME/virtual environments/virtual machines/android_x86/androidx86_remixos.img"
INSTALL_CD="$HOME/software/Operating Systems/Android/Android-x86/Remix OS/Remix_OS_for_PC_Android_M_64bit_B2016112101.iso"



qemu-system-x86_64                                                       \
  -cpu host                                                              \
  -machine accel=kvm                                                     \
  -smp 2                                                                 \
  -boot order=c                                                          \
  -drive file="$HDD",format=raw,cache=none,if=virtio,aio=native          \
  -m 2048                                                                \
  -cdrom "$INSTALL_CD"                                                   \
  -net nic,model=virtio -net tap,ifname=testing,script=no,downscript=no  \
  -vga std                                                               \
  -soundhw hda                                                           \
  -display gtk                                                           \
  -monitor stdio                                                         \
  -name "Android-x86 (Remix-OS)"
