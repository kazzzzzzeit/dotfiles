#!/bin/bash
# Location: $HOME/bin/dwm-statusbar.sh
# Description: Prints date and time information every 60 seconds with xsetroot.



while true; do
  DaySuffix() {
    case `date +%d` in
      01|21|31)  echo  "st";;
      02|22)     echo  "nd";;
      03|23)     echo  "rd";;
      *)         echo  "th";;
    esac
  }

  xsetroot -name "  $(date +"%A %-d`DaySuffix`, %I:%M%p  --  %m/%d/%Y (%B)")  "
  sleep 60
done
