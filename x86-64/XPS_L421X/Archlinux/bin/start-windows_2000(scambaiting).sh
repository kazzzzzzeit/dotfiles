#!/bin/sh
# Location: $HOME/bin/start-windows2000(scambaiting).sh
# Description: This script sets up a VM to run Windows 2000 for scambaiting.
#
# Notes:
#        00. In the unlikely event that a scammer is going to check if your
#              computer is a VM by looking at your CD-ROM in device manager
#              and knows what QEMU is you can change the value with regedt32.
#              SYSTEM -> Enum -> IDE -> CdRomQEMU_QEMU_DVD-ROM -> deviceid-> "FriendlyName"
#              (Change to "TSSTcorp DVD+/-RW TS-L633B" for example)
#
#        01. If you're concerned about the scammer noticing that your w2k box
#              is running on an i5 simply change -cpu from host to pentium3.
#
#        02. I'm emulating an Intel e1000 NIC(1GBs) but this requires
#              a driver that doesn't come with the OS. You can get this driver
#              from: https://downloadmirror.intel.com/17640/eng/PRO2K.exe
#              If you don't want to bother with that you can stick with a
#              10/100MBs NIC like the rtl8139 which works just fine by default.
#
#        03. I'm using "-vga std", this is not supported by default on Windows 2000
#              to make use of this you will need to get a 3rd party VESA driver from:
#              http://bearwindows.boot-land.net/vbempj.zip the usage of this driver
#              enables you to use MUCH higher resolutions and 32bit color!




QEMU_AUDIO_DRV=pa
HDD="$HOME/virtual environments/virtual machines/Microsoft Windows/Microsoft Windows 2000/scambaiting(LIVE).img"
INSTALL_CD="$HOME/software/Operating Systems/Microsoft Windows/Microsoft Windows 2000/Microsoft Windows 2000 Professional (5.00.2195.6717.sp4)/EN_WIN2000_PRO_SP4.ISO"



qemu-system-i386                                                          \
  -cpu host                                                               \
  -machine accel=kvm                                                      \
  -boot order=d                                                           \
  -device ide-hd,drive=drive0,model=ST320420A                             \
  -drive id=drive0,file="$HDD",format=raw,cache=none,if=none              \
  -cdrom "$INSTALL_CD"                                                    \
  -m 512                                                                  \
  -vga std                                                                \
  -soundhw ac97                                                           \
  -net nic,model=e1000 -net tap,ifname=tornet,script=no,downscript=no     \
  -usbdevice tablet                                                       \
  -rtc base=localtime                                                     \
  -display gtk                                                            \
  -monitor stdio                                                          \
  -smbios type=1,manufacturer="Dell Inc.",product="Dell Dimension"        \
  -name "Microsoft Windows 2000 Professional Edition -- FOR SCAMBAITING"
