#!/bin/bash
# Location: $HOME/bin/cd-creator.sh
# Description: An interactive shell script to create CDs with ease.
# Written by: Zadacon - zadacon@cock.li



# GLOBAL VARIABLES
RED="\e[38;5;9m"
YELLOW="\e[38;5;11m"
DARK_CYAN="\e[38;5;15m"
CYAN="\e[38;5;14m"
RESET_COLOR="$(tput sgr0)"
INVALID_OPTION_MESSAGE="${RED}ERROR:${RESET_COLOR} you have entered an invalid option, please try again."
PROJECT_NAME="NONE"
PROJECT_LOCATION="$HOME/projects/for_mom/cd_mixes"



# AVAILABLE FUNCTIONS
initial_menu()
{
  # This function displays the menu users see on start-up and
  # prompts them to make a choice from the available menu options.

  INITIAL_MENU_CHOICE=""

  echo -e "\n"
  echo "*-----------------------------------*"
  echo "|                                   |"
  echo "|           CD-CREATOR.SH           |"
  echo "|            -- by zadacon          |"
  echo "|                                   |"
  echo "*-----------------------------------*"
  echo "|                                   |"
  echo "|  (0). CREATE NEW PROJECT          |"
  echo "|  (1). MODIFY EXISTING PROJECT     |"
  echo "|                                   |"
  echo "|  (Q). EXIT                        |"
  echo "|                                   |"
  echo "*-----------------------------------*"
  echo -e "\n"

  read -p "Please input your selection: " INITIAL_MENU_CHOICE
}


new_project_menu()
{
  # This function displays the menu users see after they choose
  # the "CREATE NEW PROJECT" option from the initial menu. It
  # also displays the name of the current project you're working
  # on and prompts the user to make a choice from the available
  # "NEW PROJECT" menu options.

  NEW_PROJECT_MENU_CHOICE=""

  echo -e "\n"
  echo "*-----------------------------------*"
  echo "|                                   |"
  echo "|            NEW PROJECT            |"
  echo "|                                   |"
  echo "*-----------------------------------*"
  echo "|                                   |"
  echo "|  (0). SET PROJECT NAME            |"
  echo "|  (1). CREATE DIRECTORIES          |"
  echo "|  (2). RETRIEVE AUDIO FILES        |"
  echo "|  (3). CONVERT FILES TO WAV        |"
  echo "|  (4). NORMALIZE WAV FILES         |"
  echo "|  (5). BURN WAV FILES TO DISK      |"
  echo "|  (6). CLEAN PROJECT DIRECTORIES   |"
  echo "|                                   |"
  echo "|  (Q). RETURN TO MAIN MENU         |"
  echo "|                                   |"
  echo "*-----------------------------------*"
  echo -e "\n"

  echo -e -n "${DARK_CYAN}(CURRENT-PROJECT: ${CYAN}$PROJECT_NAME${DARK_CYAN}) ${RESET_COLOR}Please input your selection: "
  read NEW_PROJECT_MENU_CHOICE
}


set_project_name()
{
  # This function simply sets the name for the current project.
  # This will be displayed in the prompt as: "CURRENT-PROJECT: YOUR_PROJECT_NAME"

  PROJECT_NAME=""

  echo -e "\n"
  read -p "Please enter the name of your new project: " PROJECT_NAME
}


create_project_directories()
{
  # This function will check for the existence of various
  # directories used in the script and create them if
  # they don't already exist. If they do exist a warning
  # will be displayed to the user.

 if [ -d "$PROJECT_LOCATION/$PROJECT_NAME" ]; then
    echo -e "${YELLOW}WARNING:${RESET_COLOR} $PROJECT_LOCATION/$PROJECT_NAME already exists"
  else
    mkdir "$PROJECT_LOCATION/$PROJECT_NAME"
  fi
  if [ -d "$PROJECT_LOCATION/$PROJECT_NAME/original_audio_files" ]; then
    echo -e "${YELLOW}WARNING:${RESET_COLOR} $PROJECT_LOCATION/$PROJECT_NAME/original_audio_files already exists"
  else
    mkdir "$PROJECT_LOCATION/$PROJECT_NAME/original_audio_files"
  fi

  if [[ -d "$PROJECT_LOCATION/$PROJECT_NAME/wav_audio_files" ]]; then
    echo -e "${YELLOW}WARNING:${RESET_COLOR} $PROJECT_LOCATION/$PROJECT_NAME/wav_audio_files already exists"
  else
    mkdir "$PROJECT_LOCATION/$PROJECT_NAME/wav_audio_files"
  fi
}


retrieve_audio_files()
{
  # The function will CD into the:
  # $PROJECT_LOCATION/$PROJECT_NAME/original_audio_files directory and
  # prompt the user for a youtube-dl compatible URL. youtube-dl will attempt
  # to locate the media source at this url and will then download and extract
  # the audio from the media and properly name the leftover file.

  SONG_URL=""
  SONG_COUNTER="0"
  SONG_COUNTER_PADDED="$(printf "%02d" $SONG_COUNTER)"

  cd "$PROJECT_LOCATION/$PROJECT_NAME/original_audio_files"

  while [ $SONG_COUNTER -lt 18 ]; do
    echo -e "\n"
    read -p "Please enter the URL for song #$SONG_COUNTER_PADDED of 17: " SONG_URL
    youtube-dl --extract-audio --audio-format best --audio-quality 0                             \
      "$SONG_URL" -o "$SONG_COUNTER_PADDED. %(title)s-%(id)s.%(ext)s"                           &&
      echo "$SONG_COUNTER_PADDED. $SONG_URL" >> "$PROJECT_LOCATION/$PROJECT_NAME/url_list.txt"  &&
      SONG_COUNTER="$(expr $SONG_COUNTER + 1)"                                                  &&
      SONG_COUNTER_PADDED="$(printf "%02d" $SONG_COUNTER)"
  done
}


convert_to_wav()
{
  # This function will attempt to convert all files in the:
  # $PROJECT_LOCATION/$PROJECT_NAME/original_audio_files/ directory to wav format.
  # The function will then remove the extra extension(before .wav) from the files
  # in the $PROJECT_LOCATION/$PROJECT_NAME/original_audio_files/ directory.
  # After all supported files have been converted and renamed they will be
  # moved to the: $PROJECT_LOCATION/$PROJECT_NAME/wav_audio_files/ directory.

  find "$PROJECT_LOCATION"/"$PROJECT_NAME"/original_audio_files/ -type f  \
    -exec sh -c 'ffmpeg -i "{}" -vn -acodec pcm_s16le -ac 2 -ar 44100 "{}".wav' \;

  find "$PROJECT_LOCATION"/"$PROJECT_NAME"/original_audio_files/ -type f -name "*.wav"  \
    -exec sh -c 'mv "{}" "$(echo "{}" | rev | cut -d "." -f 3- | rev).wav"' \;

  mv "$PROJECT_LOCATION"/"$PROJECT_NAME"/original_audio_files/*.wav "$PROJECT_LOCATION"/"$PROJECT_NAME"/wav_audio_files/
}


normalize_wav_files()
{
  # This function will normalize all the wav files in the
  # $PROJECT_LOCATION/$PROJECT_NAME/wav_audio_files directory until
  # normalize recognizes all files as not needing normalization.

  while true; do
    if [[ ! $(normalize --mix "$PROJECT_LOCATION"/"$PROJECT_NAME"/wav_audio_files/*.wav |& grep "Applying adjustment") ]]; then
      break
    fi
  done
}


burn_wavs_to_disk()
{
  # This function will attempt to burn all wav files located in the
  # $PROJECT_LOCATION/$PROJECT_NAME/wav_audio_files directory to the
  # optical disk located at /dev/sr0.

  cdrecord -v -pad speed=1 dev=/dev/sr0 -dao -swab "$PROJECT_LOCATION"/"$PROJECT_NAME"/wav_audio_files/*.wav
}


clean_project_directories()
{
  # This function will prompt the user with a list of available
  # project directories and ask the user to choose which one
  # they would like to remove.

  CLEAN_PROJECT_DIRECTORY_CHOICE=""

  if [ ! -d "$PROJECT_LOCATION/$PROJECT_NAME" ]; then
    echo -e "${RED}ERROR:${RESET_COLOR} $PROJECT_LOCATION/$PROJECT_NAME doesn't exist!"
    return 1
  else
    echo -e "\n"
    echo "*-----------------------------------*"
    echo "|                                   |"
    echo "|        PROJECT DIRECTORIES        |"
    echo "|                                   |"
    echo "*-----------------------------------*"
    echo -e "\n"

    ls -1 "$PROJECT_LOCATION/$PROJECT_NAME/"

    echo -e "\n"
    echo -e -n "${DARK_CYAN}(CURRENT-PROJECT: ${CYAN}$PROJECT_NAME${DARK_CYAN}) ${RESET_COLOR}Enter the directory you would like to clean: "
    read CLEAN_PROJECT_DIRECTORY_CHOICE

    rm "$PROJECT_LOCATION"/"$PROJECT_NAME"/"$CLEAN_PROJECT_DIRECTORY_CHOICE"/*
  fi
}


existing_project_menu()
{
  # This function displays the menu users see after they choose
  # the "MODIFY EXISTING PROJECT" option from the initial menu.
  # It also displays the name of the current project you're working
  # on and prompts the user to make a choice from the available
  # "MODIFY EXISTING PROJECT" menu options.

  EXISTING_PROJECT_MENU_CHOICE=""

  echo -e "\n"
  echo "*-----------------------------------*"
  echo "|                                   |"
  echo "|      MODIFY EXISTING PROJECT      |"
  echo "|                                   |"
  echo "*-----------------------------------*"
  echo "|                                   |"
  echo "|  (0). CHOOSE EXISTING PROJECT     |"
  echo "|  (1). REDOWNLOAD AUDIO FILES      |"
  echo "|  (2). CONVERT FILES TO WAV        |"
  echo "|  (3). NORMALIZE WAV FILES         |"
  echo "|  (4). BURN WAV FILES TO DISK      |"
  echo "|  (5). CLEAN PROJECT DIRECTORIES   |"
  echo "|                                   |"
  echo "|  (Q). RETURN TO MAIN MENU         |"
  echo "|                                   |"
  echo "*-----------------------------------*"
  echo -e "\n"

  echo -e -n "${DARK_CYAN}(CURRENT-PROJECT: ${CYAN}$PROJECT_NAME${DARK_CYAN}) ${RESET_COLOR}Please input your selection: "
  read EXISTING_PROJECT_MENU_CHOICE

}


choose_existing_project()
{
  # This function will display an explanatory banner. The function will
  # then list the available projects in $PROJECT_LOCATION/. The user
  # will then be prompted to choose a project from those listed.

  echo -e "\n"
  echo "*-----------------------------------*"
  echo "|                                   |"
  echo "|         EXISTING PROJECTS         |"
  echo "|                                   |"
  echo "*-----------------------------------*"
  echo -e "\n"

  ls -1 "$PROJECT_LOCATION/"

  echo -e "\n"
  echo -e -n "${DARK_CYAN}(CURRENT-PROJECT: ${CYAN}$PROJECT_NAME${DARK_CYAN}) ${RESET_COLOR}Please choose a project: "
  read PROJECT_NAME
}


redownload_audio_files()
{
  # This function will backup the:
  # $PROJECT_LOCATION/$PROJECT_NAME/original_audio_files/ directory before
  # removing all the files it contained. It will then read the URLs from
  # $PROJECT_LOCATION/$PROJECT_NAME/url_list.txt and redownload them to the:
  # $PROJECT_LOCATION/$PROJECT_NAME/original_audio_files/ directory. After
  # all the songs have been re-downloaded the user will be promted to remove
  # the backup directory.

  cp -r "$PROJECT_LOCATION/$PROJECT_NAME/original_audio_files/" "$PROJECT_LOCATION/$PROJECT_NAME/original_audio_files.bak/"

  rm "$PROJECT_LOCATION"/"$PROJECT_NAME"/original_audio_files/*

  SONG_COUNTER="0"
  SONG_COUNTER_PADDED="$(printf "%02d" $SONG_COUNTER)"
  SONG_URL="$(sed -n "$(expr $SONG_COUNTER + 1)"p "$PROJECT_LOCATION"/"$PROJECT_NAME"/url_list.txt | sed "s/"$SONG_COUNTER_PADDED". //g")"

  cd "$PROJECT_LOCATION/$PROJECT_NAME/original_audio_files"

  while [ $SONG_COUNTER -lt 18 ]; do
    youtube-dl --extract-audio --audio-format best --audio-quality 0                                                                             \
      "$SONG_URL" -o "$SONG_COUNTER_PADDED. %(title)s-%(id)s.%(ext)s"                                                                           &&
      SONG_COUNTER="$(expr $SONG_COUNTER + 1)"                                                                                                  &&
      SONG_COUNTER_PADDED="$(printf "%02d" $SONG_COUNTER)"                                                                                      &&
      SONG_URL="$(sed -n "$(expr $SONG_COUNTER + 1)"p "$PROJECT_LOCATION"/"$PROJECT_NAME"/url_list.txt | sed "s/"$SONG_COUNTER_PADDED". //g")"
  done

  rm -rI "$PROJECT_LOCATION/$PROJECT_NAME/original_audio_files.bak/"
}



# MAIN FUNCTION
# This is the main function of the script. Execution begins here.
while true; do
  initial_menu
  case $INITIAL_MENU_CHOICE in
    0)
      while true; do
        new_project_menu
        case $NEW_PROJECT_MENU_CHOICE in
          0)
            set_project_name ;;
          1)
            create_project_directories ;;
          2)
            retrieve_audio_files ;;
          3)
            convert_to_wav ;;
          4)
            normalize_wav_files ;;
          5)
            burn_wavs_to_disk ;;
          6)
            clean_project_directories ;;
          Q)
            echo "Returning you to the main menu..."
            PROJECT_NAME="NONE"
            break ;;
          *)
            echo -e "$INVALID_OPTION_MESSAGE" ;;
        esac
      done ;;
    1)
      while true; do
        existing_project_menu
        case $EXISTING_PROJECT_MENU_CHOICE in
          0)
            choose_existing_project ;;
          1)
            redownload_audio_files ;;
          2)
            convert_to_wav ;;
          3)
            normalize_wav_files ;;
          4)
            burn_wavs_to_disk ;;
          5)
            clean_project_directories ;;
          Q)
            echo "Returning you to the main menu..."
            PROJECT_NAME="NONE"
            break ;;
          *)
            echo -e "$INVALID_OPTION_MESSAGE" ;;
        esac
      done ;;
    Q)
       echo -e "\n"
       echo "Goodbye..."
       exit 0 ;;
    *)
      echo -e "$INVALID_OPTION_MESSAGE" ;;
  esac
done
