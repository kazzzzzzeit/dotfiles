# Location: $HOME/.rtorrent.rc:
# Description: Configuration file for rTorrent.



# GENERAL SETTINGS

  # Close torrents when diskspace is low.
  schedule = low_diskspace,5,60,close_low_diskspace=100M

  # Check hash for finished torrents. Might be usefull until the bug is
  # fixed that causes lack of diskspace not to be properly reported.
  check_hash = yes



# LIMITATION SETTINGS

  # Maximum and minimum number of peers to connect to per torrent.
  min_peers = 40
  max_peers = 50

  # Same as above but for seeding completed torrents (-1 = same as downloading)
  min_peers_seed = 10
  max_peers_seed = 10

  # Maximum number of simultanious uploads per torrent.
  max_uploads = 5

  # Global upload and download rate in KiB. "0" for unlimited.
  download_rate = 0
  upload_rate = 0



# LOCATION SETTINGS

  # Default directory to save the downloaded torrents.
  directory = ~/torrents/output

  # Default session directory. Make sure you don't run multiple instance
  # of rtorrent using the same session directory. Perhaps using a
  # relative path?
  session = ~/torrents/sessions

  # Watch a directory for new torrents, and stop those that have been
  # deleted.
  schedule = watch_directory,5,5,load_start=~/torrents/active/*.torrent
  schedule = untied_directory,5,5,stop_untied=



# NETWORK SETTINGS

  # Port range to use for listening.
  port_range = 2790-2790

  # Start opening ports at a random position within the port range.
  port_random = no

  # Set whether the client should try to connect to UDP trackers.
  use_udp_trackers = yes

  # Encryption options, set to none (default) or any combination of the following:
  # allow_incoming, try_outgoing, require, require_RC4, enable_retry, prefer_plaintext
  encryption = allow_incoming,try_outgoing,enable_retry

  # Enable DHT support for trackerless torrents or when all trackers are down.
  # May be set to "disable" (completely disable DHT), "off" (do not start DHT),
  # "auto" (start and stop DHT as needed), or "on" (start DHT immediately).
  # The default is "off". For DHT to work, a session directory must be defined.
  dht = auto

  # UDP port to use for DHT.
  dht_port = 2781

  # Enable peer exchange (for torrents not marked private)
  peer_exchange = yes
