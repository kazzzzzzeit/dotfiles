# Location: $HOME/.mutt/accounts/example_airmail.cc
# Description: Configuration for example@airmail.cc, for use with Mutt



# GENERAL SETTINGS
set from = "example@airmail.cc"
set realname = "Example"



# IMAP SETTINGS
set imap_user = "example@airmail.cc"
set imap_pass = "YOUR PASSWORD"



# SMTP SETTINGS
set smtp_url = "smtps://example@airmail.cc@mail.cock.li:465"
set smtp_pass = "YOUR PASSWORD"



# FOLDER SETTINGS
set folder = "imaps://mail.cock.li:993"
set spoolfile = "+INBOX"
set postponed = "+Drafts"
set record = "+Sent"
