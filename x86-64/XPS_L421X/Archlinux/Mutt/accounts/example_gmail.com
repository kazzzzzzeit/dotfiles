# Location: $HOME/.mutt/accounts/example_gmail.com
# Description: Configuration for example@gmail.com, for use with Mutt



# GENERAL SETTINGS
set from = "example@gmail.com"
set realname = "Example"



# IMAP SETTINGS
set imap_user = "example@gmail.com"
set imap_pass = "YOUR PASSWORD"



# SMTP SETTINGS
set smtp_url = "smtps://example@smtp.gmail.com:465"
set smtp_pass = "YOUR PASSWORD"



# FOLDER SETTINGS
set folder = "imaps://imap.gmail.com:993"
set spoolfile = "+INBOX"
set postponed = "+[Gmail]/Drafts"
set record = "+[Gmail]/Sent Mail"
set trash = "=[Gmail]/Trash"
