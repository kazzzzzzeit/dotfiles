#!/bin/bash
# Location: $HOME/bin/mount_net_shares.sh
# Description: Decrypt 2 storage devices with the same key. Then mount them, and start NFS.



STORAGE_00="/dev/disk/by-uuid/"
STORAGE_01="/dev/disk/by-uuid/"
KEY=""



while getopts 'md' flag; do
  case "${flag}" in
    m)
      read -p "Please enter your encryption key: " KEY     &&
      echo -n "$KEY" \
        | cryptsetup -q luksOpen "$STORAGE_00" storage-00  &&
      echo -n "$KEY" \
        | cryptsetup -q luksOpen "$STORAGE_01" storage-01  &&
      mount /dev/mapper/storage-00 /mnt/storage-00         &&
      mount /dev/mapper/storage-01 /mnt/storage-01         &&
      systemctl start nfs-server
      ;;
    d)
      systemctl stop nfs-server                    &&
      umount /mnt/storage-00                       &&
      cryptsetup luksClose /dev/mapper/storage-00  &&
      umount /mnt/storage-01                       &&
      cryptsetup luksClose /dev/mapper/storage-01
      ;;
    *)
      echo "Unexpected option ${flag}"
      ;;
  esac
done
