#!/bin/bash
# Location: $HOME/bin/sync-dotfiles-to-nfs.sh
# Description: Sync dotfiles to NFS storage.



DOTFILES="projects/dotfiles/ARMv7-A/Raspberry_Pi__2B/ArchlinuxARM/"



rsync -vcrlpEtgo --delete-after --progress  \
  "$HOME/$DOTFILES"                         \
  "/mnt/storage-00/$DOTFILES"
