#!/bin/bash
# Location: $HOME/bin/backup.sh
# Description: Backup script to sync the content of storage-01 with storage-00.



rsync -vcrlpEtgo --delete-after --progress  \
  '/mnt/storage-00/'                        \
  '/mnt/storage-01/'                        \
    --exclude='Torrents/Output'
