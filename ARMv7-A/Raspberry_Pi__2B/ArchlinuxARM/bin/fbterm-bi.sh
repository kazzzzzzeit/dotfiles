#!/bin/bash
# Location: $HOME/bin/fbterm-bi.sh
# Description: Use FbTerm with a background image.



BACKGROUND_IMAGE="/mnt/storage-00/Images/Wallpapers/Single Color/grey.png"



echo -ne "\e[?25l"                &&
fbv -ciuker "$BACKGROUND_IMAGE"   &&
export FBTERM_BACKGROUND_IMAGE=1  &&
exec fbterm
