# Location: $HOME/.bash_profile
# Description: Configuration file for BASH login shells.



# DETECT SSH SESSION
if [ -n "$SSH_CLIENT" ]; then
  # APPLY SETTINGS FOR AN SSH SESSION
  TERM="xterm-color"
else
  if [ -z "$TMUX" ]; then
    # APPLY SETTINGS FOR A NON-SSH, NON-TMUX SESSION
    setterm -blank 0
    setterm -blength 0
  fi
fi



# IF EXISTS APPLY SETTINGS FROM $HOME/.bashrc
[[ -f $HOME/.bashrc ]] && . $HOME/.bashrc
