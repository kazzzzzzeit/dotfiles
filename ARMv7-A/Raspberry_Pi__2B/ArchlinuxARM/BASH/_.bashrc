# Location: $HOME/.bashrc
# Description: Configuration file for non-login BASH shells.



# PROMT COLORS
CLEAR="\[$(tput sgr0)\]"
RED="\[\033[38;5;9m\]"
CYAN="\[\033[38;5;14m\]"
DRKCYAN="\[\033[38;5;15m\]"
ORNG="\[\033[38;5;11m\]"



# ENVIRONMENT SETTINGS
PS1="$RED[$CYAN\u$RED@$DRKCYAN\h $ORNG\W$RED]$CLEAR$ "
PS2='> '
PATH=$PATH:$HOME/bin
umask 027
unset HISTFILE
LESSHISTFILE=/dev/null
VISUAL="vim"
EDITOR="vim"



# ALIAS SETTINGS
alias ls='ls -lhF --color=auto'
alias grep='grep --colour=auto'
alias fbmux='TERM=fbterm tmux'
