# Location: /boot/config.txt
# Description: System configuration parameters to be applied during the boot process.



# GENERAL SETTINGS
dtparam=audio=on
gpu_mem=16
disable_splash=1



# OVERSCAN SETTINGS
overscan_left=10
overscan_right=1
overscan_top=-2
overscan_bottom=-5



# OVERCLOCK SETTINGS
sdram_freq=450
core_freq=450
